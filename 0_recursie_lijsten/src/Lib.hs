module Lib
    ( ex1, ex2, ex3, ex4, ex5, ex6, ex7
    ) where

-- | ex1 geeft de som van een Int lijst.
ex1 :: [Int] -> Int
ex1 [] = 0
ex1 (hl : tl) = hl + ex1 tl

-- | ex2 verhoogt alle elementen van een lijst met 1
ex2 :: [Int] -> [Int]
ex2 [] = []
ex2 (hl : tl) = (hl + 1) : ex2 tl

-- | ex3 vermenigvuldigt alle elementen van een lijst met -1.
ex3 :: [Int] -> [Int]
ex3 [] = []
ex3 (hl : tl) = (-1 * hl) : ex3 tl

-- | ex4 plakt twee lijsten aan elkaar bijvoorbeeld: ([1, 2, 3] en [4, 5, 6] word [1, 2, 3, 4, 5, 6]).
ex4 :: [Int] -> [Int] -> [Int]
ex4 lijst1 [] = lijst1
ex4 lijst1 (hl2 : tl2) = ex4 (lijst1 ++ [hl2]) tl2

-- | ex5 telt twee lijsten paarsgewijs bij elkaar op.
ex5 :: [Int] -> [Int] -> [Int]
ex5 [] [] = []
ex5 (hl1 : tl1) (hl2 : tl2) = hl1 + hl2 : ex5 tl1 tl2

-- | ex6 vermenigvuldigt twee lijsten paarsgewijs met elkaar.
ex6 :: [Int] -> [Int] -> [Int]
ex6 [] [] = []
ex6 (hl1 : tl1) (hl2 : tl2) = hl1 * hl2 : ex6 tl1 tl2

-- | ex7 vermenigvuldigt twee lijsten paarsgewijs met elkaar en vervolgens de som van de overgebleven lijst.
ex7 :: [Int] -> [Int] -> Int
ex7 lijst1 lijst2 = ex1 $ ex6 lijst1 lijst2
